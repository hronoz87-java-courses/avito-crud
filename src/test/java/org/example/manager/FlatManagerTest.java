package org.example.manager;


import org.example.data.Flat;
import org.example.api.SearchRequest;
import org.example.exception.FlatNotFoundException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FlatManagerTest {
    FlatManager flatManager = new FlatManager();
    static Flat flat1 = new Flat(0, "2", 2_000_001, 30, true, false, 4, 5, false);
    static Flat flat2 = new Flat(0, FlatManager.STUDIO, 2_000_002, 20, false, true, 5, 9, false);
    static Flat flat3 = new Flat(0, "2", 2_000_000, 32, true, false, 3, 19, false);

    static Flat flatNotFirst = new Flat(0, "2", 2_000_001, 30, true, false, 1, 5, false);
    static Flat flatNotFirst2 = new Flat(0, "2", 2_000_001, 30, true, false, 5, 5, false);

    static SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
            true, false, 2, 5, 2, 19, false, false);
    static SearchRequest requestNotFirst = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
            true, false, 2, 5, 2, 19, false, false);


    @Test
    void shouldCreate() {
        List<Flat> expectedAll = new ArrayList<>();
        List<Flat> actualAll = flatManager.getAll();
        assertEquals(expectedAll, actualAll);
    }

    @Test
    void shouldByIdNull() {
        flatManager.create(flat2);
        assertThrows(FlatNotFoundException.class, () -> flatManager.getById(0));

        Flat flat1 = new Flat();
        flat1.setId(999);
        assertThrows(FlatNotFoundException.class, () -> flatManager.update(flat1));

    }

    @Test
    void shouldIdFalse() {
        flatManager.create(flat1);
        flat1.setId(1);
        flatManager.update(flat1);
    }

    @Test
    void shouldAddSingle() {
        int notExpectedId = 0;
        Flat actualCreate = flatManager.create(flat1);
        assertNotEquals(notExpectedId, actualCreate.getId());

        List<Flat> expectedAll = new ArrayList<>();
        expectedAll.add(flat1);
        List<Flat> actualAll = flatManager.getAll();
        assertEquals(expectedAll, actualAll);

        Flat actualById = flatManager.getById(actualCreate.getId());
        assertNotNull(actualById);
    }

    @Test
    void shouldAddMultiple() {
        int notExpectedId = 10;
        Flat actualCreate = flatManager.create(flat1);
        flatManager.create(flat2);
        assertNotEquals(notExpectedId, actualCreate.getId());

        List<Flat> expectedAll = new ArrayList<>();
        expectedAll.add(flat1);
        expectedAll.add(flat2);

        List<Flat> actualAll = flatManager.getAll();
        assertEquals(expectedAll, actualAll);

        Flat actualById = flatManager.getById(actualCreate.getId());
        assertNotNull(actualById);

    }

    @Test
    void shouldSearchNull() {
        SearchRequest requestFlatNull = new SearchRequest(null, null, null, null, null,
                true, false, null, null, null, null, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(requestFlatNull);
        assertEquals(expected, actual);
    }

    @Test
    void shouldSearchNoResult() {
        List<Flat> actual = flatManager.searchBy(new SearchRequest());
        assertEquals(0, actual.size());
    }

    @Test
    void shouldSearchSingleResult() {
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertEquals(expected, actual);
    }

    @Test
    void shouldSearchSingleResultArray() {
        SearchRequest request = new SearchRequest(new String[]{"2", "3"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);
        Flat flat2 = new Flat(3, "studio", 2_000_001, 30, true, false, 4, 5, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        flatManager.create(flat2);
        List<Flat> actual = flatManager.searchBy(request);
        assertEquals(expected, actual);
    }

    @Test
    void shouldSearchSingleResultFlatRoomFreeStyle() {
        SearchRequest request = new SearchRequest(new String[]{"free_style"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);
        Flat flat1 = new Flat(3, "free_style", 2_000_001, 30, true, false, 4, 5, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertEquals(expected, actual);
    }

    @Test
    void shouldSearchMultiResultFiveAndMoreFive() {
        SearchRequest request = new SearchRequest(new String[]{"5"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);

        Flat flat1 = new Flat(3, "6", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat2 = new Flat(3, "7", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat3 = new Flat(3, "free_style", 2_000_001, 30, true, false, 4, 5, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);
        expected.add(flat2);
        expected.add(flat3);

        flatManager.create(flat1);
        flatManager.create(flat2);
        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldSearchMultiResultFiveAndMoreFiveStudioAndFreeStyle() {
        SearchRequest request = new SearchRequest(new String[]{"5", "studio", "free_style"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);
        Flat flat1 = new Flat(3, "6", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat2 = new Flat(3, "studio", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat3 = new Flat(3, "free_style", 2_000_001, 30, true, false, 4, 5, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);
        expected.add(flat2);
        expected.add(flat3);

        flatManager.create(flat1);
        flatManager.create(flat2);
        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertEquals(expected, actual);
    }

    @Test
    void shouldSearchMultiResultFiveAndMoreFiveStudioAndFreeStyleNotOneResult() {
        SearchRequest request = new SearchRequest(new String[]{"6", "studio", "free_style"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);
        Flat flat1 = new Flat(3, "3", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat2 = new Flat(3, "studio", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat3 = new Flat(3, "free_style", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat4 = new Flat(3, "6", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat5 = new Flat(3, "7", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat6 = new Flat(3, "8", 2_000_001, 30, true, false, 4, 5, false);

        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);
        expected.add(flat2);
        expected.add(flat3);
        expected.add(flat4);
        expected.add(flat5);
        expected.add(flat6);

        flatManager.create(flat1);
        flatManager.create(flat2);
        flatManager.create(flat3);
        flatManager.create(flat4);
        flatManager.create(flat5);
        flatManager.create(flat6);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldSearchMultiResultNullStudioAndFreeStyleNotOneResult() {
        SearchRequest request = new SearchRequest(new String[]{"5", "studio", "free_style"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);
        Flat flat1 = new Flat(3, "2", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat2 = new Flat(3, "studio", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat3 = new Flat(3, "free_style", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat4 = new Flat(3, "6", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat5 = new Flat(3, "7", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat6 = new Flat(3, "8", 2_000_001, 30, true, false, 4, 5, false);

        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);
        expected.add(flat2);
        expected.add(flat3);
        expected.add(flat4);
        expected.add(flat5);
        expected.add(flat6);

        flatManager.create(flat1);
        flatManager.create(flat2);
        flatManager.create(flat3);
        flatManager.create(flat4);
        flatManager.create(flat5);
        flatManager.create(flat6);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldMinPriceMorePrice() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 3_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldMaxPriceLessPrice() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 1_900_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldMinSquareMoreSquare() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 31, 100,
                true, false, 2, 5, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldMaxSquareLessSquare() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 29,
                true, false, 2, 5, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldNotBalcony() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
                false, false, 2, 5, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldNotLoggia() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
                true, true, 2, 5, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldMinFloorMoreFloor() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 2_000_000, 5_000_000, 20, 100,
                true, false, 4, 5, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat3);

        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldMaxFloorLessFloor() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_900_000, 20, 100,
                true, false, 2, 2, 2, 19, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat3);

        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldNotTheFirst() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 1, 5, 1, 19, true, false);
        Flat flat3 = new Flat(6, "2", 2_000_000, 32, true, false, 1, 19, false);

        List<Flat> expected = new ArrayList<>();
        expected.add(flat3);

        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldNotTheFirst2() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 1, 5, 1, 19, true, false);
        Flat flat3 = new Flat(0, "2", 2_000_000, 32, true, false, 2, 19, false);

        List<Flat> expected = new ArrayList<>();
        expected.add(flat3);

        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertEquals(expected, actual);
    }

    @Test
    void shouldNotTheLast() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 20, 2, 20, false, true);
        Flat flat3 = new Flat(6, "2", 2_000_000, 32, true, false, 19, 19, false);

        List<Flat> expected = new ArrayList<>();
        expected.add(flat3);

        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldNotTheLast2() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 20, 2, 20, false, true);
        Flat flat3 = new Flat(6, "2", 2_000_000, 32, true, false, 2, 19, false);

        List<Flat> expected = new ArrayList<>();
        expected.add(flat3);

        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertEquals(expected, actual);
    }

    @Test
    void shouldMinTotalFloorMoreTotalFloor() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 2_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 6, 1, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldMaxTotalFloorLessTotalFloor() {
        SearchRequest request = new SearchRequest(new String[]{"2"}, 1_000_000, 5_900_000, 20, 100,
                true, false, 2, 5, 2, 4, false, false);
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);

        flatManager.create(flat1);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }


    @Test
    void shouldAddNotTheFirst() {
        List<Flat> expected = new ArrayList<>();
        expected.add(flatNotFirst);
        expected.add(flatNotFirst2);

        flatManager.create(flatNotFirst);
        flatManager.create(flatNotFirst2);
        List<Flat> actual = flatManager.searchBy(requestNotFirst);
        assertNotEquals(expected, actual);

    }

    @Test
    void shouldAddNotTheLast() {
        List<Flat> expected = new ArrayList<>();
        expected.add(flatNotFirst);
        expected.add(flatNotFirst2);

        flatManager.create(flatNotFirst);
        flatManager.create(flatNotFirst2);
        List<Flat> actual = flatManager.searchBy(requestNotFirst);
        assertNotEquals(expected, actual);

    }

    @Test
    void shouldSearchMultiplyResult() {
        List<Flat> expected = new ArrayList<>();
        expected.add(flat1);
        expected.add(flat2);
        expected.add(flat3);

        flatManager.create(flat1);
        flatManager.create(flat2);
        flatManager.create(flat3);
        List<Flat> actual = flatManager.searchBy(request);
        assertNotEquals(expected, actual);
    }

    @Test
    void shouldUpdate() {
        Flat expected = flat1;

        flatManager.create(flat1);
        Flat actual = flatManager.update(flat1);
        assertEquals(expected, actual);
    }

    @Test
    void shouldUpdateNull() {
        assertThrows(FlatNotFoundException.class, () -> flatManager.update(flat1));
    }

    @Test
    void shouldRemoveNotExistent() {
        long expectedCount = flatManager.getCount();

        flatManager.create(flat1);
        boolean actualRemove = flatManager.removeById(flat1.getId());
        assertTrue(actualRemove);

        long actualCount = flatManager.getCount();
        assertEquals(expectedCount, actualCount);
    }

    @Test
    void shouldRemoveExistent() {
        flatManager.create(flat1);
        long expectedCount = flatManager.getCount() - 1;

        boolean actualRemove = flatManager.removeById(flat1.getId());
        assertTrue(actualRemove);

        long actualCount = flatManager.getCount();
        assertEquals(expectedCount, actualCount);
    }

    @Test
    void shouldRemoveByIdNotId() {
       flatManager.create(flat1);

        boolean actual = flatManager.removeById(0);
        assertFalse(actual);
    }
}
