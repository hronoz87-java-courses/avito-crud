package org.example.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SearchRequest {
    private String[] rooms;
    private Integer minPrice;
    private Integer maxPrice;
    private Integer minSquare;
    private Integer maxSquare;
    private boolean balcony;
    private boolean loggia;
    private Integer minFloor;
    private Integer maxFloor;
    private Integer minTotalFloorInTheHouse;
    private Integer maxTotalFloorInTheHouse;
    private boolean notTheFirst;
    private boolean notTheLast;
}
